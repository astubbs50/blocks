var canvas;
var canvasPicker;
var webgl;
var webglPicker;
var mvMatrix = mat4.create();
var pMatrix = mat4.create();
var border = 0.001;
var obj3Ds;
var lt = 0;
var frames = 0;
var camera;
var movement = {
	left: false,
	right: false,
	up: false,
	back: false
};
var maxId = 0;
var spinningObjs;
var images;
var textures;

//Source code for the vertex shader
var vertexShaderStrColors = "" +
	"attribute vec3 aVertexPosition;" +
	"attribute vec4 aVertexColor;" + 
	"uniform mat4 uMVMatrix;" +
	"uniform mat4 uPMatrix;" +
	"varying vec4 vColor;" + 
	"void main(void) {" + 
	    "gl_Position = uPMatrix * uMVMatrix * vec4(aVertexPosition, 1.0);" +
	    "vColor = aVertexColor;" +
	"}";
		
//Source code for the fragment shader
var fragmentShaderStrColors = "" +
	"precision mediump float;" +
	"varying vec4 vColor;" + 
	"void main(void) {" +
	    "gl_FragColor = vColor;"+
	"}";
	
var vertexShaderStrTextures = "" +
	"attribute vec3 aVertexPosition;" +
	"attribute vec2 aTextureCoord;" + 
	"uniform mat4 uMVMatrix;" +
	"uniform mat4 uPMatrix;" +
	"varying vec2 vTextureCoord;" + 
	"void main(void) {" + 
	    "gl_Position = uPMatrix * uMVMatrix * vec4(aVertexPosition, 1.0);" +
	    "vTextureCoord = aTextureCoord;" +
	"}";
	
var fragmentShaderStrTextures = "" +
	"precision mediump float;" +
	"varying vec2 vTextureCoord;" + 
	"uniform sampler2D uSampler;" +
	"void main(void) {" +
	    "gl_FragColor = texture2D(uSampler, vec2(vTextureCoord.s, vTextureCoord.t));"+
	"}";

	
function init()
{
	//var c = getColorFromId(350);
	//alert("r: " + c[0] + " g: " + c[1] + " b: " + c[2]);
	//var i = getIdFromColor(c);
	//alert(i);
	images = ImageLoader.GetImages();
	
	camera = new Camera();
	camera.SetFreeMode();
	
	initGraphics();
    setupScene();
    run();
}

function loadImages()
{
	ImageLoader.LoadImage("textures/stone1.png");
	ImageLoader.LoadImage("textures/grass2.png");
	ImageLoader.RegisterOnReady(init, true);	
}

function initGraphics()
{
	canvas = document.getElementById("webglCanvas");
	canvas.width = canvas.offsetWidth;
	canvas.height = canvas.offsetHeight;
	canvasPicker = document.createElement("canvas");
	canvasPicker.width = canvas.width;
	canvasPicker.height = canvas.height;
	
	webgl = initWebGL(canvas);
	webgl.isTextured = true;
	webglPicker = initWebGL(canvasPicker);
	webglPicker.isTextured = false;
	
	initShaders(webgl, fragmentShaderStrColors, vertexShaderStrColors, false);
	initShaders(webgl, fragmentShaderStrTextures, vertexShaderStrTextures, true);
	initShaders(webglPicker, fragmentShaderStrColors, vertexShaderStrColors, false);
	
	textures = [];
	for(var i = 0; i < images.length; i++)
	{
		textures.push(ProcessTexture(webgl, images[i]));
	}
}

function initWebGL(canvas) 
{
	var webgl;
	
	var contextNames = ["webgl","experimental-webgl", "webkit-3d", "moz-webgl"];	
	var contextName;
	for(var i = 0; i < contextNames.length && !webgl; i++)
	{
		contextName = contextNames[i];
		try 
		{
			webgl = canvas.getContext(contextName);
			webgl.viewportWidth = canvas.width;
			webgl.viewportHeight = canvas.height;
			webgl.enable(webgl.DEPTH_TEST);
		} 
		catch (e) 
		{

		}
	}

	if (!webgl) 
	{
	    alert("Could not initialise WebGL");
	}
	else
	{
		//alert("Initialized WebGL with " + contextName);
		webgl.clearColor(0.2, 0.2, 0.9, 1);
		webgl.enable(webgl.DEPTH_TEST);
        webgl.clear(webgl.COLOR_BUFFER_BIT | webgl.DEPTH_BUFFER_BIT);
	//	webgl.clear(webgl.COLOR_BUFFER_BIT);
		webgl.viewport(0, 0, webgl.viewportWidth, webgl.viewportHeight);
		
		return webgl;
	}
}

function initShaders(webgl, fragmentShaderStr, vertexShaderStr, isTexture)
{
	//Create & compile the fragment shader
	var fragmentShader = webgl.createShader(webgl.FRAGMENT_SHADER);
	var shaderProgram;
	
	webgl.shaderSource(fragmentShader, fragmentShaderStr);
    webgl.compileShader(fragmentShader);

	if (!webgl.getShaderParameter(fragmentShader, webgl.COMPILE_STATUS)) 
	{
		alert("fragmentShader: " + webgl.getShaderInfoLog(fragmentShader));
		return null;
	}

	//Create and compile the vertex shader
	var vertexShader = webgl.createShader(webgl.VERTEX_SHADER);
	webgl.shaderSource(vertexShader, vertexShaderStr);
	webgl.compileShader(vertexShader);

	if (!webgl.getShaderParameter(vertexShader, webgl.COMPILE_STATUS)) 
	{
		alert("vertexShader: " + webgl.getShaderInfoLog(vertexShader));
		return null;
	}

	//Create the shader program
	shaderProgram = webgl.createProgram();
	webgl.attachShader(shaderProgram, vertexShader);
	webgl.attachShader(shaderProgram, fragmentShader);
	webgl.linkProgram(shaderProgram);

	//Assign the active program to this shaderProgram
	webgl.useProgram(shaderProgram);

	//Assign the vertex attribute position to the shader program
	shaderProgram.vertexPositionAttribute = webgl.getAttribLocation(shaderProgram, "aVertexPosition");
	webgl.enableVertexAttribArray(shaderProgram.vertexPositionAttribute);
	
	if(isTexture)
	{
		shaderProgram.textureCoordAttribute = webgl.getAttribLocation(shaderProgram, "aTextureCoord");
		webgl.enableVertexAttribArray(shaderProgram.textureCoordAttribute);
	}
	else
	{
		shaderProgram.vertexColorAttribute = webgl.getAttribLocation(shaderProgram, "aVertexColor");
		webgl.enableVertexAttribArray(shaderProgram.vertexColorAttribute);
	}

	//Configure the uniforms
	shaderProgram.pMatrixUniform = webgl.getUniformLocation(shaderProgram, "uPMatrix");
	shaderProgram.mvMatrixUniform = webgl.getUniformLocation(shaderProgram, "uMVMatrix");
	
	if(isTexture)
	{
		shaderProgram.samplerUniform = webgl.getUniformLocation(shaderProgram, "uSampler");
		webgl.shaderProgramTextured = shaderProgram;
	}
	else
	{
		webgl.shaderProgram = shaderProgram;
	}
}

function ProcessTexture(webgl, image)
{
	var texture = webgl.createTexture();
	webgl.bindTexture(webgl.TEXTURE_2D, texture);
	webgl.pixelStorei(webgl.UNPACK_FLIP_Y_WEBGL, true);
	//webgl.pixelStorei(webgl.UNPACK_FLIP_X_WEBGL, true);
	webgl.texImage2D(webgl.TEXTURE_2D, 0, webgl.RGBA, webgl.RGBA, webgl.UNSIGNED_BYTE, image);
	webgl.texParameteri(webgl.TEXTURE_2D, webgl.TEXTURE_MAG_FILTER, webgl.NEAREST);
	webgl.texParameteri(webgl.TEXTURE_2D, webgl.TEXTURE_MIN_FILTER, webgl.NEAREST);
	webgl.bindTexture(webgl.TEXTURE_2D, null);
	return texture;	
}

function createPlane(size, color, position, rotation, texture)
{
	var plane = {
    	obj: {},
    	picker: {},
    	sides: 1
    };
	var hs = size / 2;
	var hs2 = hs + border;
	var width = 3;
	var height = 3;
	
	var verts = [
		-hs,  0,  hs,
		 hs,  0,  hs,
		-hs,  0, -hs,
		 hs,  0, -hs 
	];
	
	var indices = [
		0, 1, 3,
		0, 3, 2
	];
	
	var colors = [
		color[0], color[1], color[2], 1,
		color[0], color[1], color[2], 1,
		color[0], color[1], color[2], 1,
		color[0], color[1], color[2], 1
	];
	
	var outline = [
		-hs2,  border,  hs2,
		 hs2,  border,  hs2,
		 hs2,  border, -hs2, 
		-hs2,  border, -hs2,
		-hs2,  border,  hs2,
	];
	
	var textureCoords = false;
	if(texture)
	{
		plane.obj.texture = texture;
		textureCoords = [ 
			 //Top
			 1.0, 0.0,
			 1.0, 1.0,
			 0.0, 0.0,
			 0.0, 1.0
		];
	}
	
	plane.id = maxId;
	maxId += plane.sides;
	
	create3dObj(webgl, plane.obj, plane.id, 0, verts, colors, indices, outline, [0, 0, 0, 1], textureCoords);
	create3dObj(webglPicker, plane.picker, plane.id, 1, verts, false, indices, false, false, false);
	
	plane.position = position;
    plane.rotation = rotation;
    
	return plane;
}


function createCube(size, colors_in, position, rotation, texture)
{
    var cube = {
    	obj: {},
    	picker: {},
    	sides: 6
    };
    var hs = size / 2;
    var hs2 = hs + 0.001;
    
    //Create the vertices
    var verts = [
    	 // Front face
        -hs, -hs,  hs,
         hs, -hs,  hs,
         hs,  hs,  hs,
        -hs,  hs,  hs,

        // Back face
        -hs, -hs, -hs,
        -hs,  hs, -hs,
         hs,  hs, -hs,
         hs, -hs, -hs,

        // Top face
        -hs,  hs, -hs,
        -hs,  hs,  hs,
         hs,  hs,  hs,
         hs,  hs, -hs,

        // Bottom face
        -hs, -hs, -hs,
         hs, -hs, -hs,
         hs, -hs,  hs,
        -hs, -hs,  hs,

        // Right face
         hs, -hs, -hs,
         hs,  hs, -hs,
         hs,  hs,  hs,
         hs, -hs,  hs,

        // Left face
        -hs, -hs, -hs,
        -hs, -hs,  hs,
        -hs,  hs,  hs,
        -hs,  hs, -hs
    ];
    
    //Create the indices
    var indices = [
        0, 1, 2,      0, 2, 3,    // Front face
        4, 5, 6,      4, 6, 7,    // Back face
        8, 9, 10,     8, 10, 11,  // Top face
        12, 13, 14,   12, 14, 15, // Bottom face
        16, 17, 18,   16, 18, 19, // Right face
        20, 21, 22,   20, 22, 23  // Left face
    ];

    //Create the outline line points.
    var outline = [
	   -hs2, -hs2,  hs2, 
	    hs2, -hs2,  hs2,
	    hs2,  hs2,  hs2,
	   -hs2,  hs2,  hs2,
	   -hs2, -hs2,  hs2,
	   -hs2, -hs2, -hs2,
	    hs2, -hs2, -hs2,
	    hs2, -hs2,  hs2,
	    hs2,  hs2,  hs2,
	    hs2,  hs2, -hs2,
	   -hs2,  hs2, -hs2,
	   -hs2,  hs2,  hs2,
	   -hs2,  hs2, -hs2,
	   -hs2, -hs2, -hs2,
	    hs2, -hs2, -hs2,
	    hs2,  hs2, -hs2
	];
	var textureCoords = false;
	
	if(texture)
	{
		cube.obj.texture = texture;
		textureCoords = [ 
	        // Front face
			0.0, 0.0,
			1.0, 0.0,
			1.0, 1.0,
			0.0, 1.0,
			
			// Back face
			1.0, 0.0,
			1.0, 1.0,
			0.0, 1.0,
			0.0, 0.0,
			
			// Top face
			0.0, 1.0,
			0.0, 0.0,
			1.0, 0.0,
			1.0, 1.0,
			
			// Bottom face
			1.0, 1.0,
			0.0, 1.0,
			0.0, 0.0,
			1.0, 0.0,
			
			// Right face
			1.0, 0.0,
			1.0, 1.0,
			0.0, 1.0,
			0.0, 0.0,
			
			// Left face
			0.0, 0.0,
			1.0, 0.0,
			1.0, 1.0,
			0.0, 1.0

         ];
	}
	else
	{
		//Create the colors
	    var colors = [];
	    for(var side = 0; side < 6; side++)
	    {
	    	for(var i = 0; i < 4; i++)
	    	{
	    		colors = colors.concat(colors_in[side]);
	    	}
	    }
	}
	
	//Create the object
	cube.id = maxId;
	maxId += cube.sides;
	create3dObj(webgl, cube.obj, cube.id, 0, verts, colors, indices, false, false, textureCoords);
	create3dObj(webglPicker, cube.picker, cube.id, 6, verts, false, indices, false, false, false);
	
    cube.position = position;
    cube.rotation = rotation;
    return cube;
}

function create3dObj(webgl, obj3D, id, sides, verts, colors, indices, outline, outlineColor, textureCoords)
{
	//Vertices
	obj3D.vbo = webgl.createBuffer();
    webgl.bindBuffer(webgl.ARRAY_BUFFER, obj3D.vbo);
    webgl.bufferData(webgl.ARRAY_BUFFER, new Float32Array(verts), 
        webgl.STATIC_DRAW);
    obj3D.vbo.itemSize = 3;
    obj3D.vbo.numItems = verts.length / 3;
    
    //Colors
    if(!colors)
    {
    	//Colors2
    	var side = 0;
    	var vertsPerSide = 4;
    	var vertCount = 0;
	    colors = [];
	    for(var i = 0; i < obj3D.vbo.numItems; i++)
	    {
	    	vertCount++;
	    	if(vertCount > vertsPerSide)
	    	{
	    		side++;
	    		vertCount = 1;
	    	}
	    	var color2 = getColorFromId(id + side);
	    	for(var j = 0; j < color2.length; j++)
	    		colors.push(color2[j]);
	    }
    }
   
    obj3D.cbo = webgl.createBuffer();
    webgl.bindBuffer(webgl.ARRAY_BUFFER, obj3D.cbo);
    webgl.bufferData(webgl.ARRAY_BUFFER, new Float32Array(colors), 
        webgl.STATIC_DRAW);
    obj3D.cbo.itemSize = 4;
    obj3D.cbo.numItems = colors.length / 4;
    
    //Indices
    obj3D.ibo = webgl.createBuffer();
    webgl.bindBuffer(webgl.ELEMENT_ARRAY_BUFFER, obj3D.ibo);
    webgl.bufferData(webgl.ELEMENT_ARRAY_BUFFER, new Uint16Array(indices), 
        webgl.STATIC_DRAW);
    obj3D.ibo.itemSize = 1;
    obj3D.ibo.numItems = indices.length;
    
    //Outline
    if(outline)
    {
	    obj3D.outvbo = webgl.createBuffer();
	    webgl.bindBuffer(webgl.ARRAY_BUFFER, obj3D.outvbo);
	    webgl.bufferData(webgl.ARRAY_BUFFER, new Float32Array(outline), 
	        webgl.STATIC_DRAW);
	    obj3D.outvbo.itemSize = 3;
	    obj3D.outvbo.numItems = outline.length / 3;
	    
	    var outlineColors = [];
	    for(var i = 0; i < obj3D.outvbo.numItems; i++)
	    {
	    	outlineColors = outlineColors.concat(outlineColor);
	    }
	    obj3D.outcbo = webgl.createBuffer();
	    webgl.bindBuffer(webgl.ARRAY_BUFFER, obj3D.outcbo);
	    webgl.bufferData(webgl.ARRAY_BUFFER, new Float32Array(outlineColors), 
	        webgl.STATIC_DRAW);
	    obj3D.outcbo.itemSize = 4;
	    obj3D.outcbo.numItems = outlineColors.length / 4;
    }
    
    if(obj3D.texture)
    {
    	obj3D.tbo = webgl.createBuffer();
    	webgl.bindBuffer(webgl.ARRAY_BUFFER, obj3D.tbo);
    	webgl.bufferData(webgl.ARRAY_BUFFER, new Float32Array(textureCoords), 
    		webgl.STATIC_DRAW);
    	obj3D.tbo.itemSize = 2;
    	obj3D.tbo.numItems = textureCoords.length / 2;
    }
}

function getColorFromId(id)
{
	var id2 = id * 3;
	var r = (id2 >> 16) & 0xFF;
	var g = (id2 >> 8) & 0xFF;
	var b = id2 & 0xFF;
	
	//alert("r: " + r + " g: " + g + " b: " + b);
	return [r / 256, g / 256, b / 256, 1];
}

function getIdFromColor(c)
{
	var rgb = (( (c[0] * 256) & 0x0ff)<<16)|(( (c[1] * 256) & 0x0ff)<<8)|( (c[2] * 256) &0x0ff);
	return Math.round(rgb / 3);
}

function setupScene()
{
	obj3Ds = [];
	/*obj3Ds.push(createCube(1, 
    	[
    		[1, 0.5, 0.2, 1],  //Front Face
    		[1, 0, 0, 1],  //Back Face
    		[0, 1, 1, 1],  //Top Face
    		[1, 0, 0, 1],  //Bottom Face
    		[0, 0, 1, 1],  //Right Face
    		[0, 1, 0, 1]   //Left Face
    	],
    	[0,  0.5, 10],
    	[0,  0,  0],
    	textures[0]
    ));
    */
    
    var width = 15;
    var height = 15;
    var offX = 2;
    var offY = 2;
    
    for(var x = 0; x < width; x++)
    {
    	for(var y = 0; y < height; y++)
    	{
    		var color = [0, 1, 0];
    		if(x > width / 2)
    			color = [1, 0, 1];
    		obj3Ds.push(createPlane(1, color, [x - offX, -1.0, y - offY], [0, 0, 0], textures[1]));
    	}
    }
    
    
    spinningObjs = [];
    
    //alert(planes.length);
}

var msg2 = "";
function run()
{
	move();
	draw(webgl, false);
	
	calcFPS();
	window.requestAnimationFrame(run);
}

function calcFPS()
{
	var d = new Date();
	var t = d.getTime();
	frames++;
	if(t - lt > 1000)
	{
		//fps = Math.round( (frames / ((t - lastTime) / 1000)) * 100) / 100;
		var fps = frames / ((t - lt) / 1000);
		document.getElementById("msg").innerHTML = "FPS: " + fps.toFixed(2);
		lt = t;
		frames = 0;
	}
}


function move()
{
	var radius = 0.1;
	if(movement.raise)
	{
		camera.position[1] += radius / 2;
		camera.Update(0,0,0);
	}
	
	if(movement.lower)
	{
		camera.position[1] -= radius / 2;
		camera.Update(0,0,0);
	}
	
	if(movement.left)
	{
		//camera.yaw += 0.05;
		//camera.Update(0, 0, 0);
		camera.position[2] -= Math.cos(camera.yaw + Math.PI / 2) * radius;
		camera.position[0] -= Math.sin(camera.yaw + Math.PI / 2) * radius;
		camera.Update(0, 0, 0);		
	}
	
	if(movement.right)
	{
		//camera.yaw -= 0.05;
		//camera.Update(0, 0, 0);	
		camera.position[2] -= Math.cos(camera.yaw - Math.PI / 2) * radius;
		camera.position[0] -= Math.sin(camera.yaw - Math.PI / 2) * radius;
		camera.Update(0, 0, 0);	
	}
	
	if(movement.up)
	{
		camera.position[2] -= Math.cos(camera.yaw) * radius;
		camera.position[0] -= Math.sin(camera.yaw) * radius;
		camera.Update(0, 0, 0);		
	}
	
	if(movement.back)
	{
		camera.position[2] += Math.cos(camera.yaw) * radius;
		camera.position[0] += Math.sin(camera.yaw) * radius;
		camera.Update(0, 0, 0);		
	}
	
	for(var i = 0; i < spinningObjs.length; i++)
	{
		obj3Ds[spinningObjs[i]].rotation[0] += 0.01;
		obj3Ds[spinningObjs[i]].rotation[1] += 0.01;
		obj3Ds[spinningObjs[i]].rotation[2] += 0.01;
	}
}

function draw(webgl, isPicker)
{
	var shaderProgram;
	if(webgl.isTextured)
	{
		shaderProgram = webgl.shaderProgramTextured;
	}
	else
	{
		shaderProgram = webgl.shaderProgram;
	}
	webgl.useProgram(shaderProgram);
	
    //Setup the viewport
	webgl.viewport(0, 0, webgl.viewportWidth, webgl.viewportHeight);
	
	//Clear the background
	webgl.clear(webgl.COLOR_BUFFER_BIT | webgl.DEPTH_BUFFER_BIT);
	
	//Setup the perspective matrix
	mat4.perspective(45, webgl.viewportWidth / webgl.viewportHeight, 0.1, 
	    100.0, pMatrix);	
	webgl.uniformMatrix4fv(shaderProgram.pMatrixUniform, false, pMatrix);
	
	var mvStart = mat4.create();
	mvMatrix = camera.GetMatrix();
	mat4.set(mvMatrix, mvStart);
		
	if(!isPicker)
	{
		for(var i = 0; i < obj3Ds.length; i++)
		{
			//mat4.identity(mvMatrix);
			mat4.set(mvStart, mvMatrix);
		    drawObject(obj3Ds[i], obj3Ds[i].obj, webgl);
		}
	}
	else
	{
		for(var i = 0; i < obj3Ds.length; i++)
		{
			//mat4.identity(mvMatrix);
			mat4.set(mvStart, mvMatrix);
		    drawObject(obj3Ds[i], obj3Ds[i].picker, webgl);
		}
	}
}

function drawObject(obj, obj3D, webgl)
{
	var shaderProgram;
	if(obj3D.texture)
	{
		shaderProgram = webgl.shaderProgramTextured;
	}
	else
	{
		shaderProgram = webgl.shaderProgram;
	}
	webgl.useProgram(shaderProgram);
	
	mat4.translate(mvMatrix, obj.position);
	mat4.rotate(mvMatrix, vec3.length(obj.rotation), obj.rotation, 
		mvMatrix);

	//Copy the mvMatrix uniform to the shader program
	webgl.uniformMatrix4fv(shaderProgram.mvMatrixUniform, false, mvMatrix);
	
	//Vertices
	webgl.bindBuffer(webgl.ARRAY_BUFFER, obj3D.vbo);
	webgl.vertexAttribPointer(shaderProgram.vertexPositionAttribute, 
		obj3D.vbo.itemSize, webgl.FLOAT, false, 0, 0);
	
	if(obj3D.texture)
	{
		//Texture
		webgl.bindBuffer(webgl.ARRAY_BUFFER, obj3D.tbo);
		webgl.vertexAttribPointer(shaderProgram.textureCoordAttribute, obj3D.tbo.itemSize, webgl.FLOAT, false, 0, 0);
		webgl.activeTexture(webgl.TEXTURE0);
		webgl.bindTexture(webgl.TEXTURE_2D, obj3D.texture);
		webgl.uniform1i(shaderProgram.samplerUniform, 0);
	}
	else
	{
		//Colors
		webgl.bindBuffer(webgl.ARRAY_BUFFER, obj3D.cbo);
		webgl.vertexAttribPointer(shaderProgram.vertexColorAttribute, 
			obj3D.cbo.itemSize, webgl.FLOAT, false, 0, 0);
	}
	//Indices
	webgl.bindBuffer(webgl.ELEMENT_ARRAY_BUFFER, obj3D.ibo);
	webgl.drawElements(webgl.TRIANGLES, obj3D.ibo.numItems, 
		webgl.UNSIGNED_SHORT, 0);
	
	//Draw the outline
	if(obj3D.outvbo)
	{
		webgl.useProgram(webgl.shaderProgram);
		webgl.uniformMatrix4fv(webgl.shaderProgram.pMatrixUniform, false, pMatrix);
		webgl.uniformMatrix4fv(webgl.shaderProgram.mvMatrixUniform, false, mvMatrix);
		
		webgl.bindBuffer(webgl.ARRAY_BUFFER, obj3D.outvbo);
		webgl.vertexAttribPointer(webgl.shaderProgram.vertexPositionAttribute, 
			obj3D.outvbo.itemSize, webgl.FLOAT, false, 0, 0);
		webgl.bindBuffer(webgl.ARRAY_BUFFER, obj3D.outcbo);
		webgl.vertexAttribPointer(webgl.shaderProgram.vertexColorAttribute, 
			obj3D.outcbo.itemSize, webgl.FLOAT, false, 0, 0);
		webgl.drawArrays(webgl.LINE_STRIP, 0, obj3D.outvbo.numItems);
	}
}


function KeyDown(e)
{	
	SetAction(e.keyCode, true);
}

function KeyUp(e)
{
	//document.getElementById("stats").innerHTML = e.keyCode;
	SetAction(e.keyCode, false);
}

function SetAction(keyCode, bOn)
{
	switch(keyCode)
	{
		//Q
		case 81:
			movement.raise = bOn;
			break;
		case 69:
			movement.lower = bOn;
			break;
		//Left
		case 65:
		case 37:
		case 100:
			movement.left = bOn;
			break;
		//Right
		case 68:
		case 39:
		case 102:
			movement.right = bOn;
			break;
		//Up
		case 87:
		case 38:
		case 104:
			movement.up = bOn;
			break;			
		case 40:
		case 83:
			movement.back = bOn;
			break;			
		//Space
		case 32:			
			//player.fire1 = bOn;
			break;
		//Enter
		case 13:			
			//player.fire2 = bOn;
			break;
	}
}

var mouseX, mouseY, lastMouseX, lastMouseY, mouseDown, mouseDownRight;

function GetMouseCoords(e)
{
	if(e.offsetX) 
	{ 
		mouseX = e.offsetX; 
		mouseY = e.offsetY; 
	} 
	else if(e.layerX) 
	{ 
		mouseX = e.layerX; 
		mouseY = e.layerY; 
	}
}

function MouseMove(e)
{
	GetMouseCoords(e);
	
	if(mouseDownRight)
	{
		if(mouseX != lastMouseX || mouseY != lastMouseY)
		{			
			//noMovementCount = 0;
			var xMove = mouseX - lastMouseX;
			var yMove = mouseY - lastMouseY;
			
			if(xMove > 0)
			{
				camera.yaw -= 0.01;
			}
			else if(xMove < 0)
			{
				camera.yaw += 0.01;
			}
			
			if(yMove > 0)
			{
				camera.pitch -= 0.01;
				if(camera.pitch < -1)
				{
					camera.pitch = -1;
				}
			}
			else if(yMove < 0)
			{
				camera.pitch += 0.01;
				if(camera.pitch > 1)
				{
					camera.pitch = 1;
				}
			}
			
			camera.Update(0, 0, 0);
			//document.getElementById("msg2").innerHTML = "yaw: " + camera.yaw + " pitch: " + camera.pitch;
		}
	}
	
	lastMouseX = mouseX;
	lastMouseY = mouseY;
}

function DetectLeftButton(e) 
{
    if ('which' in e) 
    {
        return e.which === 1;
    } 
    else if ('buttons' in e) 
    {
        return e.buttons === 1;
    } 
    else 
    {
        return e.button === 1;
    }
}

function MouseDown(e)
{
	if(DetectLeftButton(e))
	{
		mouseDown = true;
		GetMouseCoords(e);
		if(!hasPicked)
		{
			ProcessPicker();
		}
	}
	else
		mouseDownRight = true;
}

var hasPicked = false;
function ProcessPicker()
{
	hasPicked = true;
	
	//msg2 = "-----------------------------<br />"
	var picked = PickObject();
	
	//msg2 += "id: " + picked.index + " side: " + picked.side + "<br />";
	
	//document.getElementById("msg2").innerHTML += msg2;
	if( ! picked ) {
		return;
	}
	var id = picked.index;
	var obj = obj3Ds[id];
	var position = [0, 0, 0];
	
	if(obj.sides > 1)
	{
		switch(picked.side)
		{
			case 0:
				//Front
				position[0] = obj.position[0];
				position[1] = obj.position[1];
				position[2] = obj.position[2] + 1;
				break;
			case 1:
				//Back
				position[0] = obj.position[0];
				position[1] = obj.position[1];
				position[2] = obj.position[2] - 1;
				break;
			case 2:
				//Top
				position[0] = obj.position[0];
				position[1] = obj.position[1] + 1;
				position[2] = obj.position[2];
				break;
			case 3: 
				//Bottom
				position[0] = obj.position[0];
				position[1] = obj.position[1] - 1;
				position[2] = obj.position[2];
				break;
			case 4: 
				//Right
				position[0] = obj.position[0] + 1;
				position[1] = obj.position[1];
				position[2] = obj.position[2];
				break;
			case 5:
				//Left
				position[0] = obj.position[0] - 1;
				position[1] = obj.position[1];
				position[2] = obj.position[2];
				break;
		}
	}
	else
	{
		position[0] = obj.position[0];
		position[1] = obj.position[1] + 0.5;
		position[2] = obj.position[2];
	}
	
	obj3Ds.push(createCube(1, 
		[
    		[1, 0.5, 0.2, 1],  //Front Face
    		[1, 0, 0, 1],  	   //Back Face
    		[0, 1, 1, 1],  	   //Top Face
    		[1, 0, 0, 1],      //Bottom Face
    		[0, 0, 1, 1],      //Right Face
    		[0, 1, 0, 1]       //Left Face
    	],
    	position,
    	[0,  0,  0],
    	textures[0]
    ));
	
	/*
	var bFound = false;
	for(var i = 0; i < spinningObjs.length; i++)
	{
		if(spinningObjs[i] === id)
		{
			bFound = true;
			break;
		}
	}
	
	if(!bFound)
	{
		spinningObjs.push(id);
	}
	
	*/
}

function PickObject()
{
	draw(webglPicker, true);
	var canvas2 = document.createElement("canvas");
	canvas2.width = canvas.width;
	canvas2.height = canvas.height;
	var context2 = canvas2.getContext("2d");
	context2.drawImage(canvasPicker, 0, 0);
	var data = context2.getImageData(0,0, canvas.width, canvas.height).data;
	var index = ((canvas.width * mouseY) + mouseX) * 4;
	var red = data[index];
    var green = data[index + 1];
    var blue = data[index + 2];
    var alpha = data[index + 3];
    //document.body.appendChild(canvas2);
   	msg2 += "r: " + red + " g: " + green + " b: " + blue + " a: " + alpha + "<br />";
   	
    //alert(getIdFromColor([red / 256, green / 256, blue / 256, 1]));
    return GetObject(getIdFromColor([red / 256, green / 256, blue / 256, 1]));
}

function GetObject(id)
{
	for(var i = 0; i < obj3Ds.length; i++)
	{
		if(obj3Ds[i].id <= id && obj3Ds[i].id + obj3Ds[i].sides > id)
		{
			return {
				index: i,
				side: id - obj3Ds[i].id
			};
		}
	}
}

function MouseUp(e)
{
	mouseDown = false;
	mouseDownRight = false;
	hasPicked = false;
}

function MouseWheel(event)
{
	var delta = 0;

	if (!event)
		event = window.event;

	if (event.wheelDelta) 
	{ 
		delta = event.wheelDelta/120;
	} 
	else if (event.detail) 
	{ 
		delta = -event.detail/3;
	}
 
	//camera.Update(0, 0, -delta, false);
  	camera.position[1] += delta / 10;
	camera.Update(0,0,0);
	if (event.preventDefault)
		event.preventDefault();
	
	event.returnValue = false;
}
